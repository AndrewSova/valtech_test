// This is an examples of simple export.
//
// You can remove or add your own function in this file.

var PF = {
    parsleyFunc: function() {
      $(".contact-form").parsley();

      $(".contact-form").on('submit', function(e) {
          var f = $(this);
          f.parsley().validate();

          if (f.parsley().isValid()) {
              alert('This form is valid');
              $.ajax({
                  url: f.attr('action'),
                  data: f.serialize(),
                  type: 'post',
                  dataType: 'json',
                  success: function(response) {
                      alert('Congrats! Your form was successfully submitted.');
                  },
                  error: function() {
                      alert('Crap! There was something wrong.');
                  }
              });
          } else {
              alert("This form isn't valid");
          }

          e.preventDefault();
      });
  }
};

export default PF;